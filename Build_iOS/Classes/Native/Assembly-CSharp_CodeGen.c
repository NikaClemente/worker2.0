﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Game::Start()
extern void Game_Start_m87108F3ECAE225CE62F13FB4F438BBB97BB71033 ();
// 0x00000002 System.Collections.IEnumerator Game::WaitTimeForGame(System.Single)
extern void Game_WaitTimeForGame_m3B3E2E65771FD59549D55FD8AFC0DB6649B43A38 ();
// 0x00000003 System.Collections.IEnumerator Game::GetMoney(System.Single)
extern void Game_GetMoney_mEEBADB18E7F5F8D41B764BAD80B3CF6EAD963EA9 ();
// 0x00000004 System.Collections.IEnumerator Game::pauseForClick(System.Int32)
extern void Game_pauseForClick_mE3331F7CF593D8856BDD2A142F50E845D4BEFB5C ();
// 0x00000005 System.Void Game::OnClick()
extern void Game_OnClick_m4CB44F7A424061EC8C040DEC4649D112F114F8BC ();
// 0x00000006 System.Void Game::Update()
extern void Game_Update_m5C0A097D8278D1AE765DA8A0BDC865A756C9E216 ();
// 0x00000007 System.String Game::moneyFormat(System.Int64)
extern void Game_moneyFormat_m52541695916AC6A8DFBDD798A8BAAF01C75DEB00 ();
// 0x00000008 System.String Game::timeFormat(System.Int32)
extern void Game_timeFormat_m5B450405D5D36721EDB9420E5F5536A2AB09ACC7 ();
// 0x00000009 System.Void Game::.ctor()
extern void Game__ctor_mC065A45FD96CE26F2F9543F0C6BC65F2935A8C12 ();
// 0x0000000A System.Void Game_<WaitTimeForGame>d__19::.ctor(System.Int32)
extern void U3CWaitTimeForGameU3Ed__19__ctor_mA4AAC0D1C532D5A800DFB34D434D6038C07CC8EC ();
// 0x0000000B System.Void Game_<WaitTimeForGame>d__19::System.IDisposable.Dispose()
extern void U3CWaitTimeForGameU3Ed__19_System_IDisposable_Dispose_m7F13911A7C8008FD60492647D4070AEDDDAF0C12 ();
// 0x0000000C System.Boolean Game_<WaitTimeForGame>d__19::MoveNext()
extern void U3CWaitTimeForGameU3Ed__19_MoveNext_mFA85D8BB5F9CD8678DB4A4FAEA9E729835BA638D ();
// 0x0000000D System.Object Game_<WaitTimeForGame>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitTimeForGameU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04152E69F0E7C0C081AE31D053D89333EABD8910 ();
// 0x0000000E System.Void Game_<WaitTimeForGame>d__19::System.Collections.IEnumerator.Reset()
extern void U3CWaitTimeForGameU3Ed__19_System_Collections_IEnumerator_Reset_m4D34ACB3A75C6B7751A1DBB7E0D21949BBAB70A0 ();
// 0x0000000F System.Object Game_<WaitTimeForGame>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CWaitTimeForGameU3Ed__19_System_Collections_IEnumerator_get_Current_m70B68CD72CAE90A33FAD3EFA93958E9BC32DA954 ();
// 0x00000010 System.Void Game_<GetMoney>d__20::.ctor(System.Int32)
extern void U3CGetMoneyU3Ed__20__ctor_m72BB2D248A072F39C532CE8DA77F319BF6AAB857 ();
// 0x00000011 System.Void Game_<GetMoney>d__20::System.IDisposable.Dispose()
extern void U3CGetMoneyU3Ed__20_System_IDisposable_Dispose_m0BE9761895A1734E9FEE7CCA8501F7CCD26ECB46 ();
// 0x00000012 System.Boolean Game_<GetMoney>d__20::MoveNext()
extern void U3CGetMoneyU3Ed__20_MoveNext_m6443156035244ED3677D021797B87ED920EB2EF7 ();
// 0x00000013 System.Object Game_<GetMoney>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetMoneyU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DF2CAC84A846A3CF06B993836B847A70AA50231 ();
// 0x00000014 System.Void Game_<GetMoney>d__20::System.Collections.IEnumerator.Reset()
extern void U3CGetMoneyU3Ed__20_System_Collections_IEnumerator_Reset_m6675A3C50BC09A3FADADD46883B1FCAA61AEB2DB ();
// 0x00000015 System.Object Game_<GetMoney>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CGetMoneyU3Ed__20_System_Collections_IEnumerator_get_Current_mDBE3FBB028F79C4FCA323763E746DAD7A14D4E7C ();
// 0x00000016 System.Void Game_<pauseForClick>d__21::.ctor(System.Int32)
extern void U3CpauseForClickU3Ed__21__ctor_mEB340658B7F85500EB08EC9D6F489F2C714F484E ();
// 0x00000017 System.Void Game_<pauseForClick>d__21::System.IDisposable.Dispose()
extern void U3CpauseForClickU3Ed__21_System_IDisposable_Dispose_m6C7B6DB0B8DC40994B587AC0195BD9E7E5308A35 ();
// 0x00000018 System.Boolean Game_<pauseForClick>d__21::MoveNext()
extern void U3CpauseForClickU3Ed__21_MoveNext_mFB5A9C52314224B0E3B72CF7A758BEE79CABB0DF ();
// 0x00000019 System.Object Game_<pauseForClick>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpauseForClickU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A35F1E67E923C4EE854C957474AF6FD43D793AB ();
// 0x0000001A System.Void Game_<pauseForClick>d__21::System.Collections.IEnumerator.Reset()
extern void U3CpauseForClickU3Ed__21_System_Collections_IEnumerator_Reset_m39689C5CFB7975FF398F1DFE902F9B91F861BF9C ();
// 0x0000001B System.Object Game_<pauseForClick>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CpauseForClickU3Ed__21_System_Collections_IEnumerator_get_Current_m031D7FA2B31754298DA81004A8DE888906147EB2 ();
static Il2CppMethodPointer s_methodPointers[27] = 
{
	Game_Start_m87108F3ECAE225CE62F13FB4F438BBB97BB71033,
	Game_WaitTimeForGame_m3B3E2E65771FD59549D55FD8AFC0DB6649B43A38,
	Game_GetMoney_mEEBADB18E7F5F8D41B764BAD80B3CF6EAD963EA9,
	Game_pauseForClick_mE3331F7CF593D8856BDD2A142F50E845D4BEFB5C,
	Game_OnClick_m4CB44F7A424061EC8C040DEC4649D112F114F8BC,
	Game_Update_m5C0A097D8278D1AE765DA8A0BDC865A756C9E216,
	Game_moneyFormat_m52541695916AC6A8DFBDD798A8BAAF01C75DEB00,
	Game_timeFormat_m5B450405D5D36721EDB9420E5F5536A2AB09ACC7,
	Game__ctor_mC065A45FD96CE26F2F9543F0C6BC65F2935A8C12,
	U3CWaitTimeForGameU3Ed__19__ctor_mA4AAC0D1C532D5A800DFB34D434D6038C07CC8EC,
	U3CWaitTimeForGameU3Ed__19_System_IDisposable_Dispose_m7F13911A7C8008FD60492647D4070AEDDDAF0C12,
	U3CWaitTimeForGameU3Ed__19_MoveNext_mFA85D8BB5F9CD8678DB4A4FAEA9E729835BA638D,
	U3CWaitTimeForGameU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04152E69F0E7C0C081AE31D053D89333EABD8910,
	U3CWaitTimeForGameU3Ed__19_System_Collections_IEnumerator_Reset_m4D34ACB3A75C6B7751A1DBB7E0D21949BBAB70A0,
	U3CWaitTimeForGameU3Ed__19_System_Collections_IEnumerator_get_Current_m70B68CD72CAE90A33FAD3EFA93958E9BC32DA954,
	U3CGetMoneyU3Ed__20__ctor_m72BB2D248A072F39C532CE8DA77F319BF6AAB857,
	U3CGetMoneyU3Ed__20_System_IDisposable_Dispose_m0BE9761895A1734E9FEE7CCA8501F7CCD26ECB46,
	U3CGetMoneyU3Ed__20_MoveNext_m6443156035244ED3677D021797B87ED920EB2EF7,
	U3CGetMoneyU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DF2CAC84A846A3CF06B993836B847A70AA50231,
	U3CGetMoneyU3Ed__20_System_Collections_IEnumerator_Reset_m6675A3C50BC09A3FADADD46883B1FCAA61AEB2DB,
	U3CGetMoneyU3Ed__20_System_Collections_IEnumerator_get_Current_mDBE3FBB028F79C4FCA323763E746DAD7A14D4E7C,
	U3CpauseForClickU3Ed__21__ctor_mEB340658B7F85500EB08EC9D6F489F2C714F484E,
	U3CpauseForClickU3Ed__21_System_IDisposable_Dispose_m6C7B6DB0B8DC40994B587AC0195BD9E7E5308A35,
	U3CpauseForClickU3Ed__21_MoveNext_mFB5A9C52314224B0E3B72CF7A758BEE79CABB0DF,
	U3CpauseForClickU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A35F1E67E923C4EE854C957474AF6FD43D793AB,
	U3CpauseForClickU3Ed__21_System_Collections_IEnumerator_Reset_m39689C5CFB7975FF398F1DFE902F9B91F861BF9C,
	U3CpauseForClickU3Ed__21_System_Collections_IEnumerator_get_Current_m031D7FA2B31754298DA81004A8DE888906147EB2,
};
static const int32_t s_InvokerIndices[27] = 
{
	23,
	1513,
	1513,
	34,
	23,
	23,
	144,
	34,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	27,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
