﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public Text textGlobalScore;
    public Text textTimeForGame;
    public Text textGetScore;
    public Text textMoneyUp;
    public Button btnClick;
    // public Sprite btnClickImage1;
    // public Sprite btnClickImage2; 
    public Sprite clockImageHour;
    public Sprite clockImageMinute;

    public int upMoney = 50;
    public float koefTime = 1.0f;
    public int pauseTime = 3600;               // пауза в 1 час игрового времени


    private long score;                         // общий кеш
    private long timeForPlay;                   // общее время в игре
    private long money;                         // накопленные до сбора
    
    private TimeSpan ts;                        // для перевода времени
    private IEnumerator coroutineTimeForGame;   // для общего времени
    private IEnumerator coroutineMoney;         // для денег
    private IEnumerator coroutinePause;         // для неактивной кнопки

    private bool isActive = true;               // активна ли кнопка
    

    

    void Start() {
        coroutineTimeForGame = WaitTimeForGame(koefTime);
        StartCoroutine(coroutineTimeForGame);

        coroutineMoney = GetMoney(koefTime);
        StartCoroutine(coroutineMoney);
    }

    private IEnumerator WaitTimeForGame(float waitTime) {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            timeForPlay++;
            ts = TimeSpan.FromSeconds(timeForPlay);
            // clockImageHour.Transform(0f, 0f, 180f);
        }
    }

    private IEnumerator GetMoney(float waitTime) {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            // upMoney *= 2;
            money += upMoney;
        }
    }

    private IEnumerator pauseForClick(int waitTime) {
        yield return new WaitForSeconds(waitTime * koefTime);
        isActive = true;
        // btnClick.image.sprite = btnClickImage1;
    }

    

    public void OnClick() {
        if (isActive) {
            score += money;
            money = 0;
        }
        isActive = false;
        // btnClick.image.sprite = btnClickImage2;
        coroutinePause = pauseForClick(pauseTime);
        StartCoroutine(coroutinePause);
    }

    // Update is called once per frame
    void Update() {
        textGlobalScore.text = moneyFormat(score);
        textGetScore.text = moneyFormat(money);
        // textTime.text = ts.Days + "д. " + ts.Hours + "ч. " + ts.Minutes + "м. " + ts.Seconds + "с. " + ts.Milliseconds + "мс.";
        textTimeForGame.text = timeFormat(ts.Hours) + " : " + timeFormat(ts.Minutes);  // не правильный счётчик (обнуляется после 59)
        textMoneyUp.text = moneyFormat(upMoney) + "/c.";
        // setWaitTime(ts.Minutes);
    }

    private String moneyFormat(long score) {
        if (score >= 0 && score <= 999) return score + "$";
        if (score >= 1000 && score <= 999999) return Math.Round(((float)score/1000), 1) + "k$";
        if (score > 999999) return Math.Round(((float)score/1000000) , 2) + "kk$";

        return "Null";
    }

    private String timeFormat(int ts) {
        string time = ts.ToString();
        if (time.Length == 1) {
            time = '0' + time;
        }
        return time;
    }

}
